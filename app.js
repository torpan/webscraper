const path = require('path');
const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');
const express = require('express');
const { parse } = require('path');
const app = express();
const { PORT = 8080 } = process.env;

app.use(express.static( path.join(__dirname, 'public', 'static') ));

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.get('/', (req,res) => {
    return res.sendFile( path.join(__dirname, 'public', 'index.html'))
})

app.post('/submit', (req, res) => {
    // console.log(req.body.url);
    res.sendStatus(200)
   saveData(req.body.url)
})

app.listen(PORT, () => console.log('Server started on port 8080...'))

/**
 * Scrapes some data from the given url
 * and saves it to data.json
 * @param {url which user entered} dataUrl 
 */
function saveData(dataUrl) {

    request({
        method: 'GET',
        url: dataUrl
    }, (err, res, body) => {
        if (err) return console.error(err);
    
        let $ = cheerio.load(body);
    
        let h1 = $('h1');
    
        let siteSub = $('#siteSub');
    
        let p = $('p')
        // console.log(h1.text());
        let data = {
            title: h1.text(),    
            sitesub: siteSub.text(),
            paragraph: p.text()
        }
        fs.writeFileSync('data.json', JSON.stringify(data));
    });

        
        
}
