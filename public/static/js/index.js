const INPUTURL = document.getElementById("input-url")
let newlabel = document.getElementById("confirmation")

/**
 * Function called when user presses the submit button.
 * Forwards the url the user entered to the /submit endpoint
 * also creates a label that lets user know the url is ok.
 */
async function btnClicked() {
    // check if url is correct

    let newtext = document.createElement("label")
    newtext.innerHTML="URL submitted!"
    newlabel.appendChild(newtext)
    
    let dataUrl = {
        url: INPUTURL.value
    }


console.log(dataUrl);
 await fetch('/submit', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json;charset=utf-8'
    },
    body: JSON.stringify(dataUrl)
})

}


